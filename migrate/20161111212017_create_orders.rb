class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
    	t.string :status
    	t.integer :id_task
    	t.integer :id_user_employer
    	t.integer :id_user_employee

      t.timestamps
    end
  end
end
